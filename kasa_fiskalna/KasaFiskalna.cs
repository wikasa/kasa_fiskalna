﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace WindowsFormsApp1
{
    
    public partial class KasaFiskalna : Form
    {
        private void reload()//funkcja odczytująca linia po linii plik tekstowy po załadowaniu
        {
            listBox1.DataSource = File.ReadAllLines(@"..\..\list.txt");
        }
        public KasaFiskalna()
        {
            InitializeComponent();
            reload();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e) //DODAJ PRODUKT
        {
            listBox3.Items.Add(listBox1.SelectedItem);//dodanie wybranego produktu do koszyka
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)//LISTA PRODUKTÓW
        {

        }


            private void button3_Click(object sender, EventArgs e)//Przycisk +
        {
            listBox3.Items.Add(listBox3.SelectedItem);//powiela produkt w koszyku
        }

        private void button2_Click(object sender, EventArgs e) //Przycisk -
        {
            listBox3.Items.Remove(listBox3.SelectedItem);//usuwa wybrany produkt z koszyka
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)//PARAGON
        {

        }

        private void listBox3_SelectedIndexChanged(object sender, EventArgs e)//KOSZYK
        {

        }

        private void listBox4_SelectedIndexChanged(object sender, EventArgs e)//KSIAZECZKA DZIENNA
        {

        }

        private void listBox5_SelectedIndexChanged(object sender, EventArgs e)//OBROT DZIENNY
        {

        }

        private void button5_Click(object sender, EventArgs e)//ZAPLAC
        {
            string obrot2 = null;
            float obrot = 0;
            suma.SetSelected(0, true);//zaznaczenie pierwszego elementu w ListBox suma
            obrot2 = suma.SelectedItem.ToString();//przekształcenie wybranego elementu z suma na string
            obrot += float.Parse(obrot2);//sparsowanie elementu do float i dodanie do obrotu

           
            listBox5.SetSelected(0, true);//zaznaczenie pierwszego elementu w ListBox obrot
            string zmienna = listBox5.SelectedItem.ToString();//przekształcenie obrot na string
            obrot += float.Parse(zmienna);//sparsowanie do float i dodanie dawnego obrotu do aktualnego obrotu


            listBox5.Items.Clear();//wyczyszczenie ListBox obrot
            listBox5.Items.Add(obrot);//dodanie nowego obrotu

            

            listBox6.Items.Add("klient" +  " " + suma.SelectedItem);//wypisanie raportu

          
            suma.Items.Clear();//wyczyszczenie sumy
            
            listBox2.Items.Clear();//wyczyszczenie paragonu
            listBox3.Items.Clear();//wyczyszczenie koszyka

        }

        private void button4_Click(object sender, EventArgs e) //KUP
        {
           
            
         

            for (int i = 0; i < listBox3.Items.Count; i++)//dodanie w pętli wszystkich elementów z koszyka na paragon
            {
                listBox3.SetSelected(i, true);
                listBox2.Items.Add(listBox3.SelectedItem);
            }


            float sumka = 0;

            for (int i = 0; i < listBox2.Items.Count; i++)//zliczenie sumy z cen poszczególnych produktów w koszyku
            {
                listBox2.SetSelected(i, true);
                string napis = listBox2.SelectedItem.ToString().Substring(0, 5);//utworzenie string zawierającego część linii z ceną pobraną wcześniej z pliku 
                sumka += float.Parse(napis, CultureInfo.InvariantCulture.NumberFormat);//dodanie ceny produktu do sumy
                
            }
            suma.Items.Clear();//usunięcie dawnej sumy
            suma.Items.Add(sumka.ToString("n2"));//dodanie nowej sumy z precyzją dwóch miejsc po przecinku

            listBox3.Items.Clear();//wyczyszczenie koszyka

        }

        private void suma_SelectedIndexChanged(object sender, EventArgs e) // SUMA
        {
            
        }

        private void listBox6_SelectedIndexChanged(object sender, EventArgs e)//RAPORT DZIENNY
        {

        }

        private void button6_Click(object sender, EventArgs e)//PODSUMOWANIE
        {
            listBox5.SetSelected(0, true);//zaznaczenie obrotu z dnia
            listBox4.Items.Add("Koniec Dnia : "+listBox5.SelectedItem);//wypisanie obrotu do książeczki dziennej
            listBox5.Items.Clear();//wyczyszczenie obrotu
            listBox5.Items.Add("0");//dodanie 0 jako obrót

            listBox6.Items.Clear();//wyczyszczenie raprotu dziennego

        }

        private void textBox1_TextChanged(object sender, EventArgs e)//wyszukiwarka
        {
            string szukaj = textBox1.Text;//umieszczenie w szukaj tekstu wpisywanego do wyszukiwarki
            bool znalezione = false;//ustawienie, że nie znaleziono na początek
            for (int i = 0; i <= listBox1.Items.Count - 1; i++)//przeszukanie listy w petli czy zawiera string szukaj
            {
                if (listBox1.Items[i].ToString().Contains(szukaj))//znaleziono: zaznaczenie elementu i ustawienie znalezione na true
                {
                    listBox1.SetSelected(i, true);
                    znalezione = true;
                    break;
                }
            }
            if (!znalezione)//nie znaleziono
            {
                MessageBox.Show("Brak produktu");//okienko dialogowe z napisem
            }
        }
    }
}
